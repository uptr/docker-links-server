FROM nginx:alpine

COPY dockerize /usr/local/bin/
COPY index.template /opt/
COPY static/* /usr/share/nginx/html/

CMD ["dockerize", "-template", "/opt/index.template:/usr/share/nginx/html/index.html", "nginx", "-g", "daemon off;"]
