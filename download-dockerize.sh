#!/usr/bin/env sh

set -e

VERSION='v0.6.1'
TGZ_NAME=dockerize-alpine-linux-amd64-${VERSION}.tar.gz

wget https://github.com/jwilder/dockerize/releases/download/${VERSION}/${TGZ_NAME}
tar -C . -xzvf ${TGZ_NAME}
rm ${TGZ_NAME}
